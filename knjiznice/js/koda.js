
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var ehridTabela = [];

// to ni čist zihr zmer !!

// ne submita mi vedno

ehridTabela[0] = "afaf48d7-4121-45b9-be07-27423bb9d137";

ehridTabela[1] = "c98296c8-dabd-4d5a-bc5c-d68685e3158f";

ehridTabela[2] = "2cc8a9cb-5f2c-4e5c-9be1-466c857b200d";

var datumi=[];

var meritveTeze=[];

var meritveVisine=[];




var TrenutnoIme="";

var TrenutniPriimek="";

var TrenutnaTeža=0;

var TrenutnaVišina=0;


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}






/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";

  sessionID=getSessionId();

  var ime, priimek, datumRojstva, telesnaTeza, visina;

  // pa še kaj...

  if(stPacienta==1)
  {
    ime = "Dušan";
    priimek = "Strah";
    datumRojstva = "1970-01-01T00:00";

  }
  else if(stPacienta==2)
  {
    ime = "France";
    priimek = "Horvat";
    datumRojstva = "1985-01-01T00:00";

  }
  else if(stPacienta==3)
  {
    ime = "Micka";
    priimek = "Pliberšek";
    datumRojstva = "1960-01-01T00:00";

  }


  $.ajaxSetup({
    headers: {
        "Ehr-Session": sessionID
    }
  });
  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    success: function (data) {
        var ehrId = data.ehrId;
        

        //$("#header").html("EHR: " + ehrId);



        // build party data
        var partyData = {
            firstNames: ime,
            lastNames: priimek,
            dateOfBirth: datumRojstva,
            partyAdditionalInfo: [
                {
                    key: "ehrId",
                    value: ehrId
                }
                //tukaj lahko še dodam kaj : {key: "pretok zraka", value: pretokZraka}
            ]
        };
        $.ajax({
            url: baseUrl + "/demographics/party",
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(partyData),
            success: function (party) {
                if (party.action == 'CREATE') {

                    console.log("uspesno!");
                    //$("#result").html("Created: " + party.meta.href);
                   

                   // $("#obvestila").html("URL: " + party.meta.href);



                    ehridTabela[stPacienta-1] = ehrId;

                    console.log("ehrid je"+ ehrId);


                    //document.getElementById('EHRid').value=ehrId;

                
                    document.getElementById("id"+stPacienta).innerHTML="";

                    $("#id"+stPacienta).append("<p>"+ehrId+"</p>");


                    if(stPacienta==1)
                    {
                      zapisiMeritve(sessionID, ehrId, "178.4", "109.9", "2009-05-20T09:30", "36.1", "120", "82", "95" );
                      zapisiMeritve(sessionID, ehrId, "178.6", "112.0", "2011-05-20T09:30", "36.0", "125", "85", "96" );
                      zapisiMeritve(sessionID, ehrId, "178.5", "110.3", "2013-05-20T09:30", "35.9", "118", "80", "98" );
                      zapisiMeritve(sessionID, ehrId, "178.4", "108.2", "2014-05-20T09:30", "35.8", "122", "81", "97" );
                      zapisiMeritve(sessionID, ehrId, "178.4", "104.7", "2015-05-20T09:30", "36.2", "119", "79", "97" );
                      zapisiMeritve(sessionID, ehrId, "178.3", "101.5", "2016-05-20T09:30", "35.7", "115", "78", "96" );
                    }

                    else if(stPacienta==2)
                    {
                      zapisiMeritve(sessionID, ehrId, "185.5", "81.2", "2008-05-20T09:30", "36.3", "120", "84", "97" );
                      zapisiMeritve(sessionID, ehrId, "185.5", "83.3", "2010-05-20T09:30", "35.5", "115", "80", "98" );
                      zapisiMeritve(sessionID, ehrId, "185.4", "84.5", "2012-05-20T09:30", "35.8", "114", "81", "96" );
                      zapisiMeritve(sessionID, ehrId, "185.4", "86.2", "2013-04-20T09:30", "36.0", "124", "83", "95" );
                      zapisiMeritve(sessionID, ehrId, "185.5", "88.7", "2015-05-20T09:30", "35.7", "123", "81", "94" );
                      zapisiMeritve(sessionID, ehrId, "185.4", "90.2", "2016-05-20T09:30", "36.2", "119", "82", "97" );
                    }

                    else if(stPacienta==3)
                    {

                      zapisiMeritve(sessionID, ehrId, "158.4", "58.2", "2014-05-20T09:30", "35.8", "119", "83", "98" );
                      zapisiMeritve(sessionID, ehrId, "158.5", "59.5", "2015-02-20T09:30", "35.9", "114", "86", "97" );
                      zapisiMeritve(sessionID, ehrId, "158.4", "60.0", "2015-08-20T09:30", "35.5", "112", "79", "99" );
                      zapisiMeritve(sessionID, ehrId, "158.3", "57.9", "2016-02-20T09:30", "35.2", "117", "81", "97" );
                      zapisiMeritve(sessionID, ehrId, "158.4", "58.5", "2017-02-20T09:30", "35.4", "119", "82", "96" );

                    }


                }
            },

            // returne v error pa success ?

            error: function(err){
                $("#obvestila").append("<p>Napaka!</p>");


            }
        });
    }
});

   return ehrId;
}




// ----------------- !! TO MORM ŠE NAREST !! -------
function zapisiMeritve(sessionId, ehrId, telVisina, telTeza, datumUra, telTemperatura, sistTlak, diastTlak, kisikaVKrvi){

  sejniID=getSessionId();

  $.ajaxSetup({
    headers: {
        "Ehr-Session": sejniID
    }
});
  


var compositionData = {
    "ctx/time": datumUra,
    "ctx/language": "en",// treba spremenit ??
    "ctx/territory": "SI",// to tud?
    "vital_signs/body_temperature/any_event/temperature|magnitude": telTemperatura,
    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    "vital_signs/blood_pressure/any_event/systolic": sistTlak,
    "vital_signs/blood_pressure/any_event/diastolic": diastTlak,
    "vital_signs/height_length/any_event/body_height_length": telVisina,
    "vital_signs/body_weight/any_event/body_weight": telTeza
};
var queryParams = {
    "ehrId": ehrId,
    templateId: 'Vital Signs',
    format: 'FLAT',
    committer: 'Jan Adamic'
};
      $.ajax({
          url: baseUrl + "/composition?" + $.param(queryParams),
          type: 'POST',
          contentType: 'application/json',
          data: JSON.stringify(compositionData),
            success: function (res) {
            //   $("#obvestila").append("uspešno nalaganje podatkov...");
               
            },

            error: function (err) {
              $("#obvestila").append("neuspešno nalaganje podatkov...");
            }

});



}










function pridobiPodatke(ehrId){



    if(!meritveVisine[0]){
        $("#plac").append("<a class='twitter-timeline'  href='https://twitter.com/hashtag/zdravje' data-widget-id='867087988483817477'>#zdravje Tweets</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','twitter-wjs');</script>"
      );

    }

 


  var ajDI = $("#EHRid").val();

  sessionId=getSessionId();

  var uporabnik;

  ehrId=ajDI;

  if(ajDI.trim().length == 0){
      $("#obvestila").append("<p>Neveljaven ID</p>");
    }
 else {

$.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    type: 'GET',
    headers: {
        "Ehr-Session": sessionId
    },
    success: function (data) {
        var party = data.party;
        
        TrenutnoIme=party.firstNames;
        TrenutniPriimek=party.lastNames;

        $("#kreirajIme").val(party.firstNames);
    
        $("#kreirajPriimek").val(party.lastNames);

       // $("#obvestila").append(party.firstNames + ' ' +party.lastNames+":</br>");
    }
});

$.ajax({
    url: baseUrl + "/view/" + ehrId + "/weight",
    type: 'GET',
    headers: {
        "Ehr-Session": sessionId
    },
    success: function (res) {
                $.ajax({
                    url: baseUrl + "/view/" + ehrId + "/height",
                    type: 'GET',
                    headers: {
                        "Ehr-Session": sessionId
                    },
                    success: function (resVisina) {

                        $("#kreirajVisino").val(resVisina[0].height);

                        $("#kreirajTezo").val(res[0].weight);

                        TrenutnaVišina=resVisina[0].height;
                        TrenutnaTeža=res[0].weight;

                        meritveVisine=[];
                        meritveTeze=[];
                        datumi=[];

                        $("#izbiraDatuma").empty();
                        $("#izbiraDatuma").append('<option value=""></option>');

                        for (var i in resVisina) {
    
                            meritveVisine.push(resVisina[i].height);
                        }

                        for (var i in res) {
                           
                            datumi.push(res[i].time);
                            meritveTeze.push(res[i].weight);
                        }

                        var count=0;

                        for(var i in datumi){

                           //$("#obvestila").append(res[i].time + ': ' + meritveTeze[i] + res[i].unit +", "+meritveVisine[i] + resVisina[i].unit +"<br>");


                           $("#izbiraDatuma").append('<option value="'+count+'">'+datumi[count]+'</option>');

                           count++;
                        }


                    }
                });

    }
});






  }




}



function zapisiEhrIdVPolje(zaporedna){
  document.getElementById('EHRid').value=ehridTabela[zaporedna];

}

$(document).ready(function() {
  
  /*
  narisiGraf(patientBMI);

  $('#preberiObstojeciEHR').change(function() {
    $("#preberiSporocilo").html("");
    $("#preberiEHRid").val($(this).val());
  });
  */

  //$('#preberiPredlogoBolnika').change(function() {
  $('#vzorci').change(function() {
    
    $("#kreirajSporocilo").html("");
    
    var podatki = $(this).val().split(",");
    
    //$("#kreirajIme").val(podatki[0]);
    
    //$("#kreirajPriimek").val(podatki[1]);
    
    //$("#kreirajVisino").val(podatki[2]);

    //$("#kreirajTezo").val(podatki[3]);


  //  $("#obvestila").append("<p>"+podatki[0]+"</p>");


    if(podatki[0]=="Dušan"){

      zapisiEhrIdVPolje(0);
 
    }
    else if(podatki[0]=="France"){
      zapisiEhrIdVPolje(1);
      

    }
    else if(podatki[0]=="Micka"){
      zapisiEhrIdVPolje(2);
     
    }


  });


  $('#izbiraDatuma').change(function() {

    var inx = $(this).val();

    //console.log(datumi[inx]);
    //console.log(meritveTeze[inx]);
    //console.log(meritveVisine[inx]);
    //console.log(inx+" je indeks");


    //$("#avto").gg.setNeedle(10);


   // BMI = (Weight in Kilograms / (Height in Meters x Height in Meters))

   var ITM = meritveTeze[inx]/((meritveVisine[inx]*meritveVisine[inx])/10000);

   ITM=Math.round(ITM*10)/10;

              var svg=d3.select("svg");
              var g=svg.append("g").attr("transform","translate(180,140)");
              var domain = [0,100];
              
              var gg = viz.gg()
                .domain(domain)
                .outerRadius(120)
                .innerRadius(12)
                .value(0.5*(domain[1]+domain[0]))
                .duration(1000);
              
              gg.defs(svg);
              g.call(gg);  
              
              gg.setNeedle(ITM);



              d3.select(self.frameElement).style("height", "280px");

              $("#itm").empty();
              $("#itm").append("<b>"+ITM+"</b>");
              
  });


});



  // TODO: Potrebno implementirati

 


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
